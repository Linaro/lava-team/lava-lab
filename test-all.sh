#!/bin/sh

# disabled before https://git.lavasoftware.org/lava/lava/merge_requests/636 is merged
#set -e

virtualenv --python=/usr/bin/python3.7 .venv
. .venv/bin/activate
pip install --no-cache lavacli

check_health_checks() {
    instance=$1
    hc_path=$2
    lava_version=$(lavacli --uri "${instance}" system version | cut -f 1 -d '+')
    container_image="lavasoftware/amd64-lava-server:${lava_version}"
    if [ "${instance}" = "https://staging.validation.linaro.org/RPC2/" ]; then
        container_image="hub.lavasoftware.org/lava/lava/amd64/lava-server:${lava_version}"
    fi

    for FILE in ${hc_path}
    do
        echo "Checking file: $FILE"
        docker run \
            -v $PWD:/data \
            ${container_image} \
            /usr/share/lava-common/lava-schema.py \
            job \
            /data/$FILE
    done
}

# check common health checks
check_health_checks "https://validation.linaro.org/RPC2/" "shared/health-checks/*.yaml"
# check staging health checks
check_health_checks "https://staging.validation.linaro.org/RPC2/" "staging.validation.linaro.org/master-configs/staging-master.lavalab/lava-server/dispatcher-config/health-checks/*.yaml"
# check lkft health checks
check_health_checks "https://lkft.validation.linaro.org/RPC2/" "lkft.validation.linaro.org/master-configs/lkft-master.lkftlab/lava-server/dispatcher-config/health-checks/*.yaml"
# check lkft-staging health checks
check_health_checks "https://lkft-staging.validation.linaro.org/RPC2/" "lkft-staging.validation.linaro.org/master-configs/lkft-staging-master.lavalab/lava-server/dispatcher-config/*.yaml"
# check pmwg health checks
check_health_checks "https://pmwg.validation.linaro.org/RPC2/" "pmwg.validation.linaro.org/master-configs/pmwg-master.pmwglab/lava-server/dispatcher-config/health-checks/*.yaml"

# check device dictionaries
for i in " " \
	lkft \
    ledge \
    pmwg \
	staging \
	lkft-staging \
	lite \
	tf
do
    device_type_path="/usr/share/lava-server/device-types/"
	name_suffix="-master"
	prefix=${i}
	dns_name=".validation.linaro.org"
	if [ "${i}"X = " X" ]; then
		dns_name="validation.linaro.org"
		i=""
		name_suffix=master
		prefix=lava
	fi
    echo "Testing instance ${i}${dns_name}"
	version=$(lavacli --uri https://${i}${dns_name}/RPC2/ system version | cut -f 1 -d '+')
	container=lavasoftware/amd64-lava-server:${version}
	if [ "${i}" = 'lkft-staging' ]; then
		prefix=lava
	fi
	if [ "${i}" = 'staging' ]; then
		prefix=lava
	fi
	if [ "${i}" = 'ledge' ]; then
		prefix=lng
	fi
	if [ "${i}" = 'staging' ]; then
		container=hub.lavasoftware.org/lava/lava/amd64/lava-server:${version}
        device_type_path="/usr/share/lava-server/device-types/"
	fi
	echo "Instance version: ${version}"
    DDIR=${i}${dns_name}/master-configs/${i}${name_suffix}.${prefix}lab/lava-server/dispatcher-config/devices/
	docker run \
		-v $PWD:/data \
		${container} \
		/data/check-devices.py \
		--device-types "${device_type_path}" /data/shared/device-types/ \
		--devices /data/${DDIR}
    echo "***********************"
done

# cleanup virtualenv
deactivate
rm -rf .venv

