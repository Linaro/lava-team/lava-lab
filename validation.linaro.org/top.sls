base:
  '*':
    - shared.nagios
    - shared.cbrxd
    - shared.cbrxd.x86
    - common-configs

  'dispatcher*':
    - match: pcre
    - shared.nfs
    - shared.vsftpd
    - shared.bridge-utils
    - shared.apache
    - dispatcher-configs
    - dispatcher-configs.common
    - shared.cbrxd.x86
    - shared.sd-mux
    - shared.uuu.x86

  'pi-worker.*':
    - match: pcre
    - shared.nfs
    - shared.vsftpd
    - shared.bridge-utils
    - shared.apache
    - shared.uhubctl
    - dispatcher-configs
    - dispatcher-configs.common

  'master*':
    - master-configs
    - shared.master-scripts
