base:
  '*':
    - shared.nagios
    - shared.cbrxd
    - shared.cbrxd.x86
    - common-configs

  'tf-*':
    - match: pcre
    - shared.nfs
    - shared.vsftpd
    - shared.bridge-utils
    - shared.master-scripts
    - dispatcher-configs
    - dispatcher-configs.common

  'tf-pi*':
    - match: pcre
    - shared.uhubctl

  'tf-master*':
    - master-configs

