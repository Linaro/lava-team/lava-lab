#!/usr/bin/python3

#  Copyright 2019 Linaro Limited
#  Author: Dave Pigott <dave.pigott@linaro.org>

#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#
#  Check for Cambrionix hubs being available

import argparse
from cbrxapi import cbrxapi
import smtplib
import email.message as emailmessage
import socket
import jsonrpc
import pexpect


def get_port_state(hub_id, port_no):
    handle = cbrxapi.cbrx_connection_open(hub_id)
    cbrxapi.cbrx_connection_get(handle, "Port.%d.Flags" % port_no)[-1:].lower()
    cbrxapi.cbrx_connection_close(handle)


def execute_shell_command(command, timeout=10):
    proc = pexpect.spawn(command)
    if proc.expect(pexpect.EOF, timeout=timeout) != 0:
        raise RuntimeError("Failed to get response")
    response = proc.before.decode("utf-8")
    return response


def main():
    parser = argparse.ArgumentParser(description='Check PP15s are all active')

    parser.add_argument("-f", "--from_email", type=str, default="lavalab@linaro.org",
                        help="Email to send from (default=lavalab@linaro.org)")
    parser.add_argument("-i", "--ids", type=str, required=True,
                        help='Comma separated list of PP15 Hub IDs')
    parser.add_argument("-o", "--output", action="store_true",
                        help="Output debug information to stdout")
    parser.add_argument("-p", "--port", type=int, default=1,
                        help="Port to check (default=1)")
    parser.add_argument("-s", "--smtp", type=str, required=True,
                        help="SMTP Gateway address")
    parser.add_argument("-t", "--to_email", type=str, required=True,
                        help="Comma separated list of email addresses to send to")

    options = parser.parse_args()

    hub_list = options.ids.split(",")

    email_message = ""

    for hub_id in hub_list:
        try:
            get_port_state(hub_id, options.port)
            if options.output:
                print("Hub %s passed" % hub_id)
        except (jsonrpc.RPCFault, jsonrpc.RPCError, jsonrpc.RPCTransportError, ConnectionRefusedError) as e:
            if options.output:
                print("Hub %s failed" % hub_id)
            email_message += "Failed to read status for port %s for USB hub %s (%s)\n" % (options.port, hub_id, e)

    if email_message:
        execute_shell_command("/usr/sbin/service avahi-daemon restart")
        execute_shell_command("/usr/sbin/service cbrxd restart")

        msg = emailmessage.EmailMessage()
        msg['Subject'] = "PP15 problem report for %s" % socket.gethostname()
        msg.set_content(email_message)
        msg['From'] = options.from_email
        msg['To'] = options.to_email

        s = smtplib.SMTP(options.smtp)
        s.send_message(msg)
        s.quit()


if __name__ == '__main__':
    main()
