base:
  '*':
    - shared.nagios
    - shared.cbrxd
    - common-configs

  'staging0.*':
    - match: pcre
    - shared.nfs
    - shared.vsftpd
    - shared.bridge-utils
    - shared.apache
    - dispatcher-configs
    - dispatcher-configs.common

  'staging-m0.*':
    - match: pcre
    - shared.nfs
    - shared.vsftpd
    - shared.bridge-utils
    - shared.apache
    - shared.uhubctl
    - dispatcher-configs
    - dispatcher-configs.common

  'staging0[1,2].lavalab':
    - match: pcre
    - shared.cbrxd.x86
    - shared.sd-mux
    - shared.uuu.x86

  'staging03.lavalab':
    - shared.cbrxd.arm
    - shared.sd-mux
    - shared.uuu.arm

  'staging-master*':
    - shared.master-scripts
