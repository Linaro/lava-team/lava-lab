#!/bin/sh

set -e

# relies on lava-server master being checked out in ../
# used because the staging device-types are updated nightly from lava-server master.

../lava-server/share/check-devices.py --device-types ../lava-server/lava_scheduler_app/tests/device-types/ --devices staging.validation.linaro.org/master-configs/staging-master.lavalab/lava-server/dispatcher-config/devices/

