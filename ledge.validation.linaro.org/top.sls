base:
  '*':
    - shared.nagios
    - shared.cbrxd
    - shared.cbrxd.x86
    - common-configs

  'ledge*':
    - match: pcre
    - shared.nfs
    - shared.vsftpd
    - shared.bridge-utils
    - shared.master-scripts
    - shared.sd-mux
    - dispatcher-configs
    - dispatcher-configs.common

  'ledge-master*':
    - master-configs

  'ledge-dispatcher*':
    - shared.apache

