#!/usr/bin/python3

#  Copyright 2020 Linaro Limited
#  Authors: Dave Pigott <dave.pigott@linaro.org> Luca Di Stefano <luca.distefano@linaro.org>

#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#
#  Update device-dicts to use automated sync

import argparse
import contextlib
import jinja2
import os
import sys
import requests
import logging
from urllib.parse import urlsplit, urljoin


# ref: https://git.linaro.org/lava/lava-lab.git/tree/shared/lab-scripts/rerun_health_check.py#n13
class NullAuth(requests.auth.AuthBase):
    '''force requests to ignore the ``.netrc``

    Some sites do not support regular authentication, but we still
    want to store credentials in the ``.netrc`` file and submit them
    as form elements. Without this, requests would otherwise use the
    .netrc which leads, on some sites, to a 401 error.

    Use with::

        requests.get(url, auth=NullAuth())

    Copied from: https://github.com/psf/requests/issues/2773#issuecomment-174312831
    '''

    def __call__(self, r):
        return r


SYNC_KEY = "sync_to_lava"
logger = logging.getLogger()

parser = argparse.ArgumentParser(formatter_class=argparse.RawTextHelpFormatter)
parser.add_argument(
    "-d",
    "--device",
    dest="device",
    required=True,
    help="Device dictionary file to process",
)
parser.add_argument(
    "-l", "--lava-url", dest="lava", required=True, help="URL of lava instance"
)
parser.add_argument(
    "-t", "--lava-token", dest="token", required=True, help="LAVA token"
)
args = parser.parse_args()

authentication = {
    "Authorization": "Token %s" % args.token,
}
url = urlsplit(args.lava)
api_url_base = "%s://%s%s/api/v0.2/" % (url.scheme, url.netloc, url.path)
# make sure URL ens with trailing slash
if not api_url_base.endswith("/"):
    api_url_base = api_url_base + "/"

if not os.path.isfile(args.device):
    logger.error("File %s not found or no sufficient access rights" % args.device)
    sys.exit(1)
filename = args.device
hostname = os.path.basename(filename).split(".", 1)[0]
device_type = None

with open(filename, "r") as jinja_config_file:
    jinja_config = jinja_config_file.read()
    env = jinja2.Environment(autoescape=False)
    ast = env.parse(jinja_config)
    extends = ast.find(jinja2.nodes.Extends)
    if extends:
        device_type = extends.template.value.split(".", 1)[0]
        print(device_type)
    sync = list(ast.find_all(jinja2.nodes.Assign))
    for node in sync:
        with contextlib.suppress(AttributeError):
            if node.target.name == SYNC_KEY:
                # there already is a sync_to_lava dict
                logger.info("Device dictionary already has %s" % SYNC_KEY)
                sys.exit(0)
    # create sync_to_lava dictionary
    if device_type is None:
        logger.error("Unable to parse device_type")
        sys.exit(1)

device_list_request = requests.get(
    urljoin(api_url_base, "devices"), params={"hostname": hostname}, headers=authentication, auth=NullAuth()
)
if device_list_request.status_code == 200:
    device_list = device_list_request.json()["results"]
    if len(device_list) != 1:
        # there should only be 1
        logger.error("No devices with name %s found in %s" % (hostname, api_url_base))
        sys.exit(1)
    device = device_list[0]
    worker = device["worker_host"]
    tags = []
    aliases = []
    for tag in device["tags"]:
        tag_request = requests.get(urljoin(api_url_base, "tags/%s/" % tag), headers=authentication, auth=NullAuth())
        if tag_request.status_code == 200:
            tag_value = tag_request.json()["name"]
            tags.append(tag_value)
        else:
            logger.error("Can't request tag {}: {}".format(tag, tag_request.status_code))
    # fixme: alias filtering is broken in the API
    aliases_request = requests.get(urljoin(api_url_base, "aliases/"), headers=authentication, auth=NullAuth())
    if aliases_request.status_code == 200:
        for alias in aliases_request.json()["results"]:
            logger.info("Checking alias {}".format(alias))
            if alias["device_type"] == device_type:
                logger.info("Adding alias {}".format(alias))
                aliases.append(alias["name"])
    else:
        logger.error("Can't request aliases {}".format(aliases_request.status_code))

    with open(filename, "a") as jinja_config_file:
        sync_to_lava = {
            "device_type": device_type,
            "worker": worker,
            "tags": tags,
            "aliases": aliases,
        }
        print(sync_to_lava)
        jinja_config_file.write("\r\n")
        jinja_config_file.write("{%% set sync_to_lava = %s %%}" % (sync_to_lava))
else:
    logger.error("Unable to authenticate with LAVA %s" % api_url_base)
    logger.error(device_list_request.status_code)
    logger.error(device_list_request.text)
