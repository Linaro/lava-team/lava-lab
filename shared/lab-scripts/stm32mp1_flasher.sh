#!/bin/bash

usage() {
    echo "Usage: $0 <layout file> <tarball>" >&2
    exit 1
}

if test ! $# -eq 2; then
    usage
    exit
fi

lsusb
pwd
tree
cat $1
tar xvf $2
# make the script compatible with old health check
gunzip *.wic.bin.gz | true
/usr/local/bin/STM32_Programmer_CLI -c port=usb1 -w $1 -q
