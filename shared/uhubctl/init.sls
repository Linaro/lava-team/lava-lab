libusb-1.0-0-dev:
  pkg:
    - installed

uhubctl:
  cmd.run:
    - name: git clone git://github.com/mvp/uhubctl.git /opt/uhubctl
    - creates: /opt/uhubctl

make-uhubctl:
  cmd.run:
    - name: make
    - cwd: /opt/uhubctl

/usr/bin/uhubctl:
  file.managed:
    - source: /opt/uhubctl/uhubctl
    - mode: 755
