base:
  '*':
    - shared.nagios
    - shared.cbrxd
    - shared.cbrxd.x86
    - shared.pmwg-scripts
    - common-configs

  'pmwg-*':
    - match: pcre
    - shared.nfs
    - shared.vsftpd
    - shared.bridge-utils
    - dispatcher-configs
    - dispatcher-configs.common

  'pmwg-master*':
    - master-configs
