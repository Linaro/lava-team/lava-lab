cbrxd:
  archive.extracted:
    - name: /usr/local
    - source: salt://shared/cbrxd/arm/cbrxd-arm-0.11.0b742.tar.gz
    - user: root
    - group: root
    - require:
      - pkg: make
      - pkg: gcc
      - pkg: avahi-daemon
      - pkg: libudev-dev

install-cbrxd:
  cmd.run:
    - name: /usr/local/share/cbrxd/setup/install_service.sh
    - cwd: /usr/local/share/cbrxd/setup/
    - onchanges:
      - cbrxd
