#!/usr/bin/python3
#  Copyright 2016 Linaro Limited
#  Author: Dave Pigott <dave.pigott@linaro.org>

#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#
# Command line control for the Robot Electronics ETH008
#  - http://www.robot-electronics.co.uk/htm/eth008tech.htm
# exec(open("./eth008_control").read())

import sys
from socket import (socket, AF_INET, SOCK_STREAM)
from time import sleep
import argparse


class EthernetRelay(object):
    def __init__(self, ip, port, relay_number):
        self.connected = False
        if ip is None or relay_number is None:
            raise ValueError("You must specify an ip address, relay number and state")

        if relay_number < 1 or relay_number > 8:
            raise ValueError("Relay number %s invalid. Must be 1->8" % relay_number)

        self.socket = socket(AF_INET, SOCK_STREAM)
        self._open(ip, port)
        self.relay_number = relay_number
        self.control = {"off": self.control_off,
                        "on": self.control_on,
                        "offon": self.control_off_on,
                        "onoff": self.control_on_off,
                        "status": self.get_relay_status}
        self.delay = 1
        self.connected = True

    def __del__(self):
        if self.connected:
            self._close()

    def set_relay_on(self):
        self._write("\x20%s\x00" % chr(self.relay_number))
        self._read(1)
        print("Relay %s now on" % self.relay_number)

    def set_relay_off(self):
        self._write("\x21%s\x00" % chr(self.relay_number))
        self._read(1)
        print("Relay %s now off" % self.relay_number)

    def get_module_info(self):
        self._write("\x10")
        response = self._read(3)
        return {"id": response[0], "hw": response[1], "fw": response[2]}

    def get_relay_status(self):
        self._write("\x24")
        relay_status = self._read(1)
        # assuming output is little endian
        # there is just one byte so should not matter
        if int.from_bytes(relay_status, "little") & (1 << self.relay_number - 1) != 0:
            print("Relay %s is on" % self.relay_number)
        else:
            print("Relay %s is off" % self.relay_number)

    def control_relay(self, state, delay):
        self.delay = delay
        try:
            self.control[state]()
        except KeyError:
            raise ValueError("State must be 'on', 'off', 'onoff' or 'offon")

    def control_on(self):
        self.set_relay_on()

    def control_off(self):
        self.set_relay_off()

    def control_on_off(self):
            self.set_relay_on()
            sleep(self.delay)
            self.set_relay_off()

    def control_off_on(self):
            self.set_relay_off()
            sleep(self.delay)
            self.set_relay_on()

    def _open(self, ip, port):
        try:
            self.socket.connect((ip, port))
        except Exception as err:
            print("Failed to connect: %s" % err)
            raise
        try:
            self._check_module()
        except Exception as err:
            print("Exception: %s" % err)
            raise

    def _read(self, num_chars):
        response = self.socket.recv(num_chars)
        if response == '':
            print("Error reading message")
            raise IOError("socket connection broken")
        return response

    def _write(self, command):
        try:
            self.socket.sendall(command.encode())
        except Exception as err:
            print("Failed to write: %s" % err)
            raise

    def _close(self):
        try:
            self.socket.close()
        except Exception as e:
            print("Failed to close: %s" % e)

    def _check_module(self):
        module_info = self.get_module_info()
        if module_info["id"] != 0x13:
            raise IOError("This is not the module you are looking for: %s" % module_info)


def main():
    parser = argparse.ArgumentParser(description='eth008 ethernet relay control')

    parser.add_argument("-a", "--address", type=str, required=True,
                        help="IP address or hostname of the eth008")
    parser.add_argument("-d", "--delay", type=int, default=1,
                        help="Delay in seconds between off/on or on/off switch (default=1)")
    parser.add_argument("-p", "--port", type=int, default=17494,
                        help="Socket port to communicate through (default 17494)")
    parser.add_argument("-r", "--relay", type=int, required=True, metavar="RELAY_NUMBER", choices=range(1, 9),
                        help="Number of the relay to control (1-8)")
    parser.add_argument("-s", "--state", type=str, required=True, choices=["off", "on", "offon", "onoff", "status"],
                        help="State of the relay (off/on/offon/onoff)")

    options = parser.parse_args()

    try:
        connection = EthernetRelay(options.address, options.port, options.relay)
        connection.control_relay(options.state, options.delay)
    except ValueError as e:
        print(e)
        parser.print_help()
        sys.exit(1)


if __name__ == '__main__':
    main()
