#!/usr/bin/python3

#  Copyright 2016 Linaro Limited
#  Author: Maria Högberg <maria.hogberg@linaro.org>

#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#
# Check if available memory is hitting limits

import psutil
import argparse
import sys


def main():
    parser = argparse.ArgumentParser(description='Check free memory')

    parser.add_argument("-w", "--warning", type=int, choices=range(1,101), metavar="(1-100)", default=80,
                        help="percentage level at which to issue a warning (default 80)")
    parser.add_argument("-c", "--critical", type=int, choices=range(1,101), metavar="(1-100)", default=90,
                        help="percentage level at which to issue a critical warning (default 90)")

    options = parser.parse_args()

    if options.warning >= options.critical:
        print("Warning level must be lower than critical level")
        sys.exit(-1)

    mem_used = 100 * (psutil.virtual_memory().total - psutil.virtual_memory().available) /psutil.virtual_memory().total

    if mem_used >= options.critical:
        print("Memory critical: %s" % mem_used)
        sys.exit(2)

    if mem_used >= options.warning:
        print("Memory warning: %s" % mem_used)
        sys.exit(1)

    print("All good: %s" % mem_used)

    sys.exit(0)


if __name__ == '__main__':
    main()
