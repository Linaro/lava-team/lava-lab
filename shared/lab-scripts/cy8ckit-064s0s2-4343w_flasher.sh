#!/bin/sh
set -ex

OPENOCD_PATH="/usr/local/bin/openocd"
OPENOCD="${OPENOCD_PATH}/bin/openocd"

usage() {
    echo "Usage: $0 <CMSIS-DAP Device serial number> <SPE image> <NSPE image>" >&2
    exit 1
}

[ $# -ne 3 ] && usage

SERIAL_NO=$1
SPE_IMG=$2
NSPE_IMG=$3

${OPENOCD} \
        -s ${OPENOCD_PATH}/scripts \
        -c "source [find interface/kitprog3.cfg]" \
        -c "source [find target/psoc6_2m_secure.cfg]" \
        -c "cmsis_dap_serial ${SERIAL_NO}" \
        -c "init; reset init" \
        -c "echo {** Erasing SP **}" \
        -c "flash erase_address 0x101bc000 0x10000" \
        -c "echo {** Flashing {${SPE_IMG}} **}" \
        -c "if [catch {program {${SPE_IMG}}} ] { echo {** Program operation failed **} } else { echo {** SPE image flashed successfully **} }" \
        -c "echo {** Flashing {${NSPE_IMG}} **}" \
        -c "if [catch {program {${NSPE_IMG}}} ] { echo {** Program operation failed **} } else { echo {** NSPE image flashed successfully **} }" \
        -c "reset halt; exit 0"
