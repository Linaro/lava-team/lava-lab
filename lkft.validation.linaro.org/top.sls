base:
  '*':
    - shared.nagios
    - shared.cbrxd
    - shared.cbrxd.x86
    - common-configs

  'lkft-slave*':
    - match: pcre
    - shared.nfs
    - shared.vsftpd
    - shared.bridge-utils
    - shared.apache
    - dispatcher-configs
    - dispatcher-configs.common

  'lkft-qemu*':
    - match: pcre
    - shared.nfs
    - shared.vsftpd
    - shared.bridge-utils
    - shared.apache
    - dispatcher-configs.common

  'lkft-master*':
    - master-configs
    - shared.master-scripts
