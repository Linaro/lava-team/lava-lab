#!/bin/sh
set -ex

OPENOCD_PATH="/usr/local/bin/openocd"
OPENOCD="${OPENOCD_PATH}/bin/openocd"

usage() {
    echo "Usage: $0 <CMSIS-DAP Device serial number>" >&2
    exit 1
}

[ $# -ne 1 ] && usage

SERIAL_NO=$1

echo "Reset device ${SERIAL_NO}\n"

${OPENOCD} \
        -s ${OPENOCD_PATH}/scripts \
        -c "source [find interface/kitprog3.cfg]" \
        -c "source [find target/psoc6_2m_secure.cfg]" \
        -c "cmsis_dap_serial ${SERIAL_NO}" \
        -c "init; reset run; exit 0"
