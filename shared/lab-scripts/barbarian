#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# vim: set ts=4
#
# Copyright 2021-present Linaro Limited
#
# SPDX-License-Identifier: MIT

import argparse
import contextlib
import select
import socket
import sys
import termios
import time
import tty

import paramiko

#############
# Constants #
#############
__version__ = "0.1"

ACTIONS = {
    "On": "btn",
    "Off": "forceOff",
    "SoftReset": "system",
}

ACTIONS_IPMITOOL = {
    "On": "on",
    "Off": "off",
    "SoftReset": "reset",
}


###########
# Helpers #
###########
class SilentPolicy(paramiko.MissingHostKeyPolicy):
    def missing_host_key(self, client, hostname, key):
        pass


def interactive(chan):
    oldtty = termios.tcgetattr(sys.stdin)
    try:
        tty.setraw(sys.stdin.fileno())
        tty.setcbreak(sys.stdin.fileno())
        chan.settimeout(0.0)

        while True:
            r, w, e = select.select([chan, sys.stdin], [], [])
            if chan in r:
                try:
                    x = chan.recv(1024).decode("utf-8")
                    if len(x) == 0:
                        raise Exception("Connection closed")
                    sys.stdout.write(x)
                    sys.stdout.flush()
                except socket.timeout:
                    pass
            if sys.stdin in r:
                x = sys.stdin.read(1)
                if len(x) == 0:
                    break
                if x == "\x1d":
                    break
                chan.send(x)

    finally:
        termios.tcsetattr(sys.stdin, termios.TCSADRAIN, oldtty)


##########
# Parser #
##########
def setup_parser() -> argparse.ArgumentParser:
    parser = argparse.ArgumentParser(
        prog="barbarian", description="Conan the barbarian"
    )

    parser.add_argument(
        "--version", action="version", version=f"%(prog)s, {__version__}"
    )

    group = parser.add_argument_group("BMC")
    group.add_argument(
        "--host",
        metavar="IP",
        type=str,
        required=True,
        help="Host IP",
    )
    group.add_argument(
        "--port",
        type=str,
        default="22",
        help="Port",
    )
    group.add_argument(
        "--user",
        type=str,
        required=True,
        help="Username",
    )
    group.add_argument(
        "--pass",
        type=str,
        required=True,
        help="Password",
        dest="password",
    )
    group.add_argument(
        "--command",
        type=str,
        default="kudo.sh",
        help="BMC command",
    )

    parser.add_argument("--retry", type=int, default=5, help="Automatically retry")
    parser.add_argument(
        "actions",
        nargs="+",
        choices=sorted(list(ACTIONS.keys()) + ["Connect", "Reset"]),
    )

    return parser


def connect(options: argparse.Namespace) -> paramiko.SSHClient:
    client = paramiko.SSHClient()
    client.load_system_host_keys()
    client.set_missing_host_key_policy(SilentPolicy())
    client.connect(
        options.host, options.port, options.user, options.password, timeout=5
    )
    client.get_transport().set_keepalive(5)
    return client


############
# Hanlders #
############
def handle_connect(options: argparse.Namespace) -> None:
    while True:
        with contextlib.suppress(Exception):
            client = connect(options)
            channel = client.invoke_shell()
            data = channel.recv(len("root@")).decode("utf-8")
            assert data == "root@"
            channel.sendall(
                "obmc-console-client -c /etc/obmc-console/server.ttyS1.conf\n"
            )
            interactive(channel)
            channel.sendall(b"\x1d")
            channel.close()
            client.close()
            assert input("telnet> ") == "quit"
            break
        print("\nRetrying")


def handle_reset(options: argparse.Namespace) -> None:
    with contextlib.suppress(Exception):
        client = connect(options)
        print("Resetting the node")
        if options.command == "ipmitool":
            (stdin, _, _) = client.exec_command("/usr/bin/ipmitool power cycle")
        else:
            (stdin, _, _) = client.exec_command("/usr/sbin/kudo.sh rst hotswap")
        stdin.channel.recv_exit_status()
        client.close()
        time.sleep(5)

    while True:
        with contextlib.suppress(Exception):
            print("Trying to connect to the BMC")
            client = connect(options)
            (stdin, _, _) = client.exec_command("ls")
            stdin.channel.recv_exit_status()
            client.close()
            print("-> OK")
            break
        print("-> NOK, retrying")
        time.sleep(10)


def handle_others(options: argparse.Namespace, action: str) -> None:
    retries = options.retry + 1
    while retries:
        retries -= 1
        client = connect(options)
        if options.command == "ipmitool":
            (stdin, _, _) = client.exec_command(f"/usr/bin/ipmitool power {ACTIONS_IPMITOOL[action]}")
        else:
            (stdin, _, _) = client.exec_command(f"/usr/sbin/kudo.sh rst {ACTIONS[action]}")
        if stdin.channel.recv_exit_status() == 0:
            return
        client.close()
        print("Retrying after 5s")
        time.sleep(5)


##############
# Entrypoint #
##############
def main() -> int:
    parser = setup_parser()
    options = parser.parse_args()

    # ssh configuration
    for action in options.actions:
        if action == "Connect":
            handle_connect(options)
        elif action == "Reset":
            handle_reset(options)
        else:
            handle_others(options, action)
    return 0


if __name__ == "__main__":
    sys.exit(main())
