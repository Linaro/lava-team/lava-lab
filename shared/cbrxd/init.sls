avahi-daemon:
  pkg:
    - installed

libudev-dev:
  pkg:
    - installed

python3-dev:
  pkg:
    - installed

gcc:
  pkg:
    - installed

make:
  pkg:
    - installed

jsonrpc:
  archive.extracted:
    - name: /opt
    - source: salt://shared/cbrxd/jsonrpc-0.1-py3.tar.gz
    - user: root
    - group: root
    - require:
      - pkg: python3-dev

install-jsonrpc:
  cmd.run:
    - name: python3 setup.py install
    - cwd: /opt/jsonrpc-0.1
