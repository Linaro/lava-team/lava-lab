LAVA Lab Scripts

Controlling Cambrionix USB Hubs - cbrxd_hub_control
Have to have cbrxd and avahi-daemon installed

  -a DELAY, --sleep_after DELAY
                        Amount of time in seconds to wait after executing
                        command (default 0)
  -b DELAY, --sleep_before DELAY
                        Amount of time in sedonds to wait before executing
                        command (default 0)
  -d DELAY, --delay DELAY
                        Amount of time in sedonds to wait after executing
                        second mode command (default 10)
  -g, --get_state       Get port state
  -i HUB_ID, --hub_id HUB_ID
                        The Cambrionix Hub ID (e.g. DJ00JOZ2)
  -m MODE, -m1 MODE, --mode1 MODE, --mode MODE
                        The first mode to set the port to
  -m2 MODE, --mode2 MODE
                        The second (optional) mode to set the port to after
                        the delay
  -u USB_PORT, --usb_port USB_PORT
                        USB port on the hub to control/get state


Controlling snmp PDUs - snmp_pdu_control
  --command {off,on,reboot,status}
                        What you wish to do with the port
  --delay DELAY         Delay in seconds when rebooting between power off and
                        power on (default 10 seconds)
  --hostname PDU        The pdu you wish to control - e.g. pdu05 or the IP
                        address
  --mib_name MIB_NAME   The PDU MIB identity string (default "PowerNet-
                        MIB::sPDUOutletCtl.")
  --port PORT_NUMBER    The pdu port you wish to control, e.g. 4 or 15
  --retries RETRIES     Number of times to retry setting the port (default=3)
  --sleep_after SLEEP_AFTER
                        Delay in seconds after command is executed (defaul 0)
  --sleep_before SLEEP_BEFORE
                        Delay in seconds before command is executed (default
                        0)
  --timeout TIMEOUT     Time in seconds to wait for command to fail (default
                        10 seconds)

Debugging devices - develop-lxc


Test job reporting - testjobsummary
  -d DIRECTORY, --directory DIRECTORY
                        Top directory for report hierarchy (default is current
                        dir)
  -f FROM_EMAIL, --from_email FROM_EMAIL
                        Email address to be reported as sender,
                        default='lavareports@linaro.org'
  -g GATEWAY, --gateway GATEWAY
                        SMTP Gateway to send email through
  -h HOST, --host HOST  LAVA instance path, e.g. validation.linaro.org
  -j JOBFILTER, --jobfilter JOBFILTER
                        Command separated list of job descriptions to filter
                        on
  -l LIST, --list LIST  List of email addresses to send the report to
  -m MAPFILE, --mapfile MAPFILE
                        Location of the message mapping file
  -o, --output_progress
                        Output progress information to stderr
  -p PERIOD, --period PERIOD
                        Time period to report on in minutes (default 10)
  -r REPORT_WORD, --report_word REPORT_WORD
                        Report word for email - e.g. Daily, Weekly

Job storage management
lava-server manage jobs rm --older-than 365d

Talking to an instance - lavacli
~/.config/lavacli.yaml

Checking hubs - pp15scheck

