#!/bin/bash
set -ex

JLink_TOOL_PATH="/usr/local/SEGGER/JLink_V754a"
JLINK="${JLink_TOOL_PATH}/JLinkExe"

usage() {
    echo "Usage: $0 -c [reset|flash] -s <Device serial number> [-f <Firmware tarball>]" >&2
    exit 1
}

reset_board() {
    # Create a temporary jlink command file
    reset_cmd=$(mktemp)
    echo -e "r\ngo\nexit" > ${reset_cmd}

    ${JLINK} -device lpc55s69 -if swd -speed 2000 -autoconnect 1 -SelectEmuBySN ${BOARD_SN} -commanderscript ${reset_cmd}
    rm -f ${reset_cmd}
}

flash_fw() {
    # Beside the binaries, there is a JLink command script 'flash.jlink' included in the firmware tarball
    tar xf ${FW_TARBALL}
    ls -lh
    cat flash.jlink

    ${JLINK} -device lpc55s69 -if swd -speed 2000 -autoconnect 1 -SelectEmuBySN ${BOARD_SN} -commanderscript flash.jlink
}

while getopts "s:f:c:" argv
do
    case ${argv} in
        s)
            BOARD_SN=${OPTARG}
            ;;
        f)
            FW_TARBALL=${OPTARG}
            ;;
        c)
            CMD=${OPTARG}
            ;;
        *)
            usage
            ;;
    esac
done

[ -z "${CMD}" -o -z "${BOARD_SN}" ] && usage

case ${CMD} in
    reset)
        reset_board
        ;;
    flash)
        if [ ! -f "${FW_TARBALL}" ]; then
            echo "No firmware file found!"
            usage
        fi
        flash_fw
        ;;
    *)
        usage
        ;;
esac
