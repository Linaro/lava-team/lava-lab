base:
  '*':
    - shared.nagios
    - shared.cbrxd
    - shared.cbrxd.x86
    - common-configs

  'lava*-morello-project.org':
    - match: pcre
    - shared.nfs
    - shared.vsftpd
    - shared.bridge-utils
    - shared.master-scripts
    - dispatcher-configs
    - dispatcher-configs.common

  'morello-pi*':
    - match: pcre
    - shared.uhubctl

  'lava.morello-project.org*':
    - master-configs

