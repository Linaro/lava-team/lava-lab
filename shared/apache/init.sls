/etc/apache2/sites-available/lava-dispatcher.conf:
  file.copy:
    - source: /usr/share/lava-dispatcher/apache2/lava-dispatcher.conf
    - force: True

a2dissite 000-default:
  cmd.run

a2ensite lava-dispatcher:
  cmd.run

apache2:
  pkg:
    - latest
  service.running:
    - enable: True
    - reload: True
    - watch:
      - file: /etc/apache2/sites-available/lava-dispatcher.conf

