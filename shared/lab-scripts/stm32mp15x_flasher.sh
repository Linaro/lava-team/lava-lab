#!/bin/bash

STM32_TOOL_PATH="/usr/local/STMicroelectronics/STM32Cube_2.14/STM32CubeProgrammer/bin"
STM32_PROG_CLI="${STM32_TOOL_PATH}/STM32_Programmer_CLI"

usage() {
    echo "Usage: $0 -l <layout file> -f <tarball> -s <board id> [-r <replacement binary>]" >&2
    exit 1
}

while getopts ":l:f:s:r:" argv
do
    case ${argv} in
        l)
            PART_LAYOUT=${OPTARG}
            ;;
        f)
            FW_TARBALL=${OPTARG}
            ;;
        s)
            BOARD_SN=${OPTARG}
            ;;
        r)
            REP_BIN=${OPTARG}
            ;;
        :)
            ;;
        *)
            usage
            ;;
    esac
done

[ -z "${FW_TARBALL}" -o -z "${PART_LAYOUT}" -o -z "${BOARD_SN}" ] && usage

timeout 2 ${STM32_PROG_CLI} -l usb

pwd
tree
cat "${PART_LAYOUT}"

echo "*** Untar the firmware ${FW_TARBALL} ***"
tar xvf "${FW_TARBALL}"

if [ -n "${REP_BIN}" -a -f "${REP_BIN}" ]; then
    echo "*** Replace the binary with ${REP_BIN} ***"
    $(file "${REP_BIN}" | grep -q "compressed data") && tar xvf "${REP_BIN}" || cp "${REP_BIN}" .
fi


${STM32_PROG_CLI} -c port=usb1 sn="${BOARD_SN}" -w "${PART_LAYOUT}" -q
