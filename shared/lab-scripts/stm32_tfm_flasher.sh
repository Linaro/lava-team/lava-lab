#!/bin/bash
set -ex

STM32_TOOL_PATH="/usr/local/STMicroelectronics/STM32Cube_2.14/STM32CubeProgrammer/bin"
EXTERNAL_LOADER_DIR="${STM32_TOOL_PATH}/ExternalLoader/"
export PATH=${STM32_TOOL_PATH}:${PATH}

usage() {
    echo "Usage: $0 -c [reset|flash] -s <board SN> -f <tarball>" >&2
    exit 1
}

reset_board() {
    echo "Reset the board"
    STM32_Programmer_CLI -q -c port=SWD sn=${BOARD_SN} HWrst
}

flash_tfm_fw() {
    local retry="True"
    local max_retry_times=3
    local retry_time=0

    echo -e "Firmware tarball: ${FW_TARBALL}\n"

    tar xvf ${FW_TARBALL}

    # Disable showing the progress bar while flashing the firmware,
    # Otherwise it might cause LAVA hang
    f=$(grep -rl "STM32_Programmer_CLI" * 2>/dev/null)
    sed -i "s/\"STM32_Programmer_CLI\"/\"STM32_Programmer_CLI -q\"/" ${f}

    [ ! -f "./TFM_UPDATE.sh" ] && echo "Can not find firmware update script!!" && exit 1

    # Override PATH and external_loader
    sed -i "s/^PATH=.*//" *.sh
    sed -i "s|C:.*\\ExternalLoader\\\|${EXTERNAL_LOADER_DIR}|" TFM_UPDATE.sh

    set +e
    # We don't have regression.sh in the health check firmware
    if [ -f "./regression.sh" ]; then
        bash -x ./regression.sh ${BOARD_SN}
    fi
    [ $? -eq 0 ] && bash -x ./TFM_UPDATE.sh ${BOARD_SN} && retry="False"
    # Retry flash the firmware if failed
    while [ "${retry}" == "True" ];
    do
        retry_time=$((retry_time+1))
        [ ${retry_time} -gt ${max_retry_times} ] && echo "Trying to flash firmware ${retry_time} times, failed" && exit 1
        echo "Retry flash firmware #${retry_time} time"
        sleep 2
        # We don't have regression.sh in the health check firmware
        if [ -f "./regression.sh" ]; then
            bash -x ./regression.sh ${BOARD_SN}
        fi
        [ $? -eq 0 ] && bash -x ./TFM_UPDATE.sh ${BOARD_SN} && retry="False"
    done
    set -e
}

while getopts "s:f:c:" argv
do
    case ${argv} in
        s)
            BOARD_SN=${OPTARG}
            ;;
        f)
            FW_TARBALL=${OPTARG}
            ;;
        c)
            CMD=${OPTARG}
            ;;
        *)
            usage
            ;;
    esac
done

[ -z "${CMD}" -o -z "${BOARD_SN}" ] && usage

case ${CMD} in
    reset)
        reset_board
        ;;
    flash)
        if [ -z "${FW_TARBALL}" ]; then
            echo "No firmware file specified."
            usage 
        fi
        flash_tfm_fw
        ;;
    *)
        usage
        ;;
esac
