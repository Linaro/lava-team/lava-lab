base:
  '*':
    - shared.nagios
    - shared.cbrxd
    - shared.cbrxd.x86
    - common-configs

  'lkft-*':
    - match: pcre
    - shared.nfs
    - shared.vsftpd
    - shared.bridge-utils
    - shared.master-scripts
    - dispatcher-configs.common

  'lkft-staging-master*':
    - master-configs
    - dispatcher-configs
