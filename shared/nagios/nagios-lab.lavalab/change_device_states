#!/usr/bin/env python3

#  Copyright 2021 Linaro Limited
#  Author: Dave Pigott <dave.pigott@linaro.org>

#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#
#  Find all devices in a particular instance that are in the Bad state
# and change them to Unknown - up to a maximum number of times in which
# case put it in Maintenence.

import argparse
import logging
import os
import requests
import sys
from netrc import netrc
from urllib import parse
from urllib.parse import urlsplit


class NullAuth(requests.auth.AuthBase):
    def __call__(self, r):
        return r

def main():
    parser = argparse.ArgumentParser(description="Change devices in one state to a new state", add_help=False)
    parser.add_argument("--host", required=True, help="LAVA host url")
    parser.add_argument("--limit", default=3, help="Number of failed health check before putting into Maintenance")
    parser.add_argument("--token", default=os.environ.get("LAVA_TOKEN"), help="LAVA access token")
    parser.add_argument("--loglevel", default="INFO", choices=["DEBUG", "INFO", "WARNING", "ERROR", "CRITICAL"],
            help="Log level")
    parser.add_argument("--username", default="localadmin", help="LAVA username associated with the token")

    args=parser.parse_args()
    logging.basicConfig(level=logging.getLevelName(args.loglevel))

    host_parts = parse.urlparse(args.host)
    host = "%s://%s" % (host_parts.scheme, host_parts.netloc)
    
    if args.token == None or args.username == None:
        try:
            netrcauth = netrc()
            username, _, token = netrcauth.authenticators(host_parts.netloc)
        except FileNotFoundError:
            logging.warning(".netrc file not available")
            sys.exit(1)
        except TypeError:
            logging.warning("No token available for this instance in .netrc")
            sys.exit(1)
        except:
            logging.warning("Unknown error parssing .netrc")
            sys.exit(1)
    else:
        username = args.username
        token = args.token

    auth_dict = {"Authorization": "Token %s" % token}

    device_url = parse.urljoin(host, "api/v0.2/devices/?health=Bad")
    logging.debug("Requesting %s" % device_url)
    
    devices = requests.get(device_url, headers=auth_dict, auth=NullAuth())

    if devices.status_code == 200:
        for device in devices.json()["results"]:
            device_name = device["hostname"]
            hc_url = parse.urljoin(host, "api/v0.2/jobs/?actual_device=%s&submitter__username=lava-health&ordering=-id&limit=%s" % (device_name, args.limit))
            logging.debug("Requesting %s" % hc_url)
            hcs = requests.get(hc_url, headers=auth_dict, auth=NullAuth())
            if hcs.status_code == 200:
                device_health = "Maintenance"
                for health_check in hcs.json()["results"]:
                    if health_check["health"] == "Complete":
                        device_health = "Unknown"
                        break;
                device_url = parse.urljoin(host, "api/v0.2/devices/%s/" % device["hostname"])
                device["health"] = device_health
                device_request = requests.put(device_url, device, headers=auth_dict, auth=NullAuth())

                if device_request.status_code == 200:
                    logging.info("Set %s from Bad to %s" % (device["hostname"], device_health))
                else:
                    logging.warning("Failed to set %s from Bad to %s (%s)" % 
                            (device["hostname"], device_health, device_request.status_code))

if __name__ == '__main__':
    main()

