# keep important /etc configs (e.g. ser2net and lava-slave)
# WARNING: Do not, ever, set clean to True. If you do you
# will completely blow away the whole of your /etc tree

/etc/lava-server/env.yaml:
  file.managed:
    - source: salt://master-configs/{{ grains['id'] }}/lava-server/env.yaml

/etc/lava-server/lava-server-gunicorn:
  file.managed:
    - source: salt://master-configs/{{ grains['id'] }}/lava-server/lava-server-gunicorn

/etc/lava-server/dispatcher-config/devices:
  file.recurse:
    - source: salt://master-configs/{{ grains['id'] }}/lava-server/dispatcher-config/devices
    - clean: True
    - include_empty: True

/etc/lava-server/dispatcher-config/health-checks:
  file.recurse:
    - source: salt://master-configs/{{ grains['id'] }}/lava-server/dispatcher-config/health-checks
    - clean: True
    - include_empty: True

/etc/crontab.master:
  file.managed:
    - source: salt://master-configs/{{ grains['id'] }}/crontab

reload-master-crontab:
  cmd.run:
    - name: crontab /etc/crontab.master
    - watch:
      - file: /etc/crontab.master

