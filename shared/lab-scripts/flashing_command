#!/bin/bash

usage() {
      echo "Usage:"
      echo "    $0 -h                  Display this help message."
      echo "    $0 -l <layout>         Layout file."
      echo "    $0 -i <image>          Image file. In case layout is presend bmaptool is used. dd is used when no layout exists"
      echo "    $0 -t <dev>            Target device"
      exit 0
}

while getopts ":l:i:t:h" opt; do
  case ${opt} in
    l) LAYOUT_FILE="${OPTARG}"
    ;;
    i) IMAGE_FILE="${OPTARG}"
    ;;
    t) TARGET_DEVICE="${OPTARG}"
    ;;
    h) usage && exit 1
    ;;
    *) echo "Invalid option -${OPTARG}" >&2
    usage && exit 1
    ;;
  esac
done

if [ $OPTIND -eq 1 ]; then
    echo "No options were passed"
    usage
    exit 1
fi

if [ -z "${TARGET_DEVICE}" ]; then
    echo "Target device empty"
    exit 1
fi

if [ -z "${IMAGE_FILE}" ] || [ "${IMAGE_FILE}" = "{IMAGE}" ]; then
    echo "Image file empty"
    exit 1
fi

if [ -z "${LAYOUT_FILE}" ] || [ "${LAYOUT_FILE}" = "{LAYOUT}" ]; then
    echo "No layout file, using dd"
    dd if="${IMAGE_FILE}" of="${TARGET_DEVICE}" bs=1M oflag=sync conv=nocreat
else
    echo "Layout file provided, using bmaptool"
    bmaptool -q copy --bmap "${LAYOUT_FILE}" "${IMAGE_FILE}" "${TARGET_DEVICE}"
fi
exit 0
