#!/usr/bin/python3

#  Copyright 2019 Linaro Limited
#  Author: Dave Pigott <dave.pigott@linaro.org>

#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#
#  Setup a server as a LAVA remote worker


import apt
import argparse
import os
import re
import subprocess
import sys


class WorkerEnvironment(object):
    def __init__(self, instance, master_port, log_port, cert_dir, lava_slave_file, workername):
        super(WorkerEnvironment, self).__init__()
        self.instance = instance
        self.master_port = master_port
        self.log_port = log_port
        self.cert_dir = cert_dir
        self.lava_slave_file = lava_slave_file
        self.workername = workername
        self.release_number = int(execute_shell_command("lsb_release -r").split()[1])
        self.release_name = execute_shell_command("lsb_release -c").split()[1].decode("utf-8")

    def install_dispatcher(self):
        lava_apt_file = "/etc/apt/sources.list.d/lava.list"

        if not os.path.exists(lava_apt_file):
            with open(lava_apt_file, "w") as file:
                file.write("deb http://apt.lavasoftware.org/release/ %s main\n" % self.release_name)

        print(execute_shell_command("curl -fsSL https://apt.lavasoftware.org/lavasoftware.key.asc | apt-key add -"))
        cache = apt.cache.Cache()
        cache.update()
        cache.open()

        pkg = cache["lava-dispatcher"]
        if pkg.is_installed:
            pkg.mark_upgrade()
        else:
            pkg.mark_install()

        try:
            cache.commit()
        except Exception as e:
            print("lava-dispatcher instalation failed: %s" % e)

    def generate_certificate(self):
        execute_shell_command("/usr/share/lava-dispatcher/create_certificate.py %s" % self.workername)

        print("")
        print("****************************************************************")
        print("Now you must get the public certificate for the lava master instance\n"
              "'%s' and copy it to\n"
              "%s." %
              (self.instance, self.cert_dir))
        print("")
        print("****************************************************************")
        print("You also need to send your public certificate\n"
              "%s/%s.key\n"
              "to the LAVA admins of your instance" %
              (self.cert_dir, self.workername))
        print("****************************************************************")

    def configure_lava_slave(self):
        with open(self.lava_slave_file, "r") as file:
            config = file.read()

        config = re.sub('# MASTER_URL=.*', 'MASTER_URL="tcp://%s:%s"' % (self.instance, self.master_port), config)
        config = re.sub('# LOGGER_URL=.*', 'LOGGER_URL="tcp://%s:%s"' % (self.instance, self.log_port), config)
        config = re.sub('# ENCRYPT=.*', 'ENCRYPT="--encrypt"', config)
        config = re.sub('# MASTER_CERT=.*', 'MASTER_CERT="--master-cert %s/master.key"' % self.cert_dir, config)
        config = re.sub('# SLAVE_CERT=".*', 'SLAVE_CERT="--slave-cert %s/%s.key_secret"' %
                        (self.cert_dir, self.workername), config)

        with open(self.lava_slave_file, "w") as file:
            file.write(config)

        execute_shell_command("systemctl restart lava-slave")


def execute_shell_command(command):
    return subprocess.check_output(command, shell=True)


def main():
    parser = argparse.ArgumentParser(description='Install a server as a LAVA remote worker')

    parser.add_argument("-i", "--instance", type=str, required=True,
                        help="The LAVA instance that you wish to connect to")
    parser.add_argument("-l", "--log_port", type=int, default=5555,
                        help="Logger port on the LAVA instance")
    parser.add_argument("-m", "--master_port", type=int, default=5556,
                        help="Master port on the LAVA instance")
    parser.add_argument("-w", "--workername", type=str, required=True,
                        help="The name the remote worker will be known by")

    options = parser.parse_args()

    if os.geteuid() != 0:
        print("You have to run %s as root" % os.path.basename(sys.argv[0]))
        exit(1)

    environment = WorkerEnvironment(options.instance,
                                    options.master_port, options.log_port,
                                    "/etc/lava-dispatcher/certificates.d",
                                    "/etc/lava-dispatcher/lava-slave",
                                    options.workername)

    if environment.release_number < 9:
        print("LAVA is only supported on Debian versions >= 8 (jesssie) - please upgrade your system and retry")
        exit(2)

    environment.install_dispatcher()

    environment.generate_certificate()

    environment.configure_lava_slave()


if __name__ == '__main__':
    main()
