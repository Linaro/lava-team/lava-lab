#!/usr/bin/python3

#  Copyright 2016-2022 Linaro Limited
#  Author: Dave Pigott <dave.pigott@linaro.org>
#  Author: Luca Di Stefano  <luca.distefano@linaro.org>

#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#
#  Unifi POE Control

import pexpect
import argparse
import time


########################################################################################################################

class UnifiPOECtrl(object):
    def __init__(self, sw_ip, sw_port, delay, sleep_before, sleep_after, timeout, retries, ssh_ident, ssh_user,
                 ssh_port, prompt_ssh, prompt_switch_cli):
        super(UnifiPOECtrl, self).__init__()
        self._sw_ip = sw_ip
        self._sw_port = "0/" + str(sw_port)
        self._delay = delay
        self._sleep_before = sleep_before
        self._sleep_after = sleep_after
        self._timeout = timeout
        self._retries = retries
        self._ssh_ident = ssh_ident
        self._ssh_user = ssh_user
        self._ssh_port = ssh_port
        self._prompt_ssh = prompt_ssh
        self._prompt_switch_cli = prompt_switch_cli
        self._poe_action = {"On": "opmode auto", "Off": "opmode shutdown"}

    def _connect_ssh(self):
        connection = pexpect.spawn("ssh -i {} -p {} unifi@{}".format(self._ssh_ident, self._ssh_port, self._sw_ip))
        connection.expect(self._prompt_ssh)
        connection.sendline("telnet localhost\n")
        connection.expect(self._prompt_switch_cli)
        connection.sendline("enable\n")
        connection.expect(self._prompt_switch_cli)
        return connection

    def status(self):
        # TODO -- Status method
        p = self._connect_ssh()
        p.sendline("\n")
        p.expect(self._prompt_switch_cli)
        p.sendline("show version\n")
        p.expect(self._prompt_switch_cli)

        version = p.before.decode("utf-8")
        print(version)

        p.sendline("show poe port info {} | include {}\n".format(self._sw_port, self._sw_port))
        # p.expect(self._prompt_switch_cli)
        p.expect("\n")
        response = p.before
        # print(response_lines)
        print(str(response))
        if "Delivering Power" in response:
            return "ON"
        elif "Searching" in response:
            return "ON"
        elif "Disabled" in response:
            return "OFF"
        else:
            raise Exception("Unexpected response from switch: %s" % response)

    def get_port_state(self):
        status = self.status()
        if status == "ON":
            print("Port status is on(%s)" % status)
        elif status == "OFF":
            print("Port status is off(%s)" % status)
        else:
            print("Port status is unknown(%s)" % status)

    def _set_port(self, state):
        p = self._connect_ssh()
        poecmd = "poe " + self._poe_action[state]
        p.sendline("configure\n")
        p.expect('(UBNT)*')
        p.sendline("interface {}".format(self._sw_port))
        p.sendline(poecmd + "\n")
        p.expect('(UBNT)*')
        p.sendline("end\n")
        p.close()

    def power_on(self):
        if self._sleep_before != 0:
            time.sleep(self._sleep_before)
        self._set_port('On')
        if self._sleep_after != 0:
            time.sleep(self._sleep_after)
        print("%s port %s now powered on" % (self._sw_ip, self._sw_port))

    def power_off(self):
        if self._sleep_before != 0:
            time.sleep(self._sleep_before)
        self._set_port("Off")
        if self._sleep_after != 0:
            time.sleep(self._sleep_after)
        print("%s port %s now powered off" % (self._sw_ip, self._sw_port))

    def reboot(self):
        if self._sleep_before != 0:
            time.sleep(self._sleep_before)
        self.power_off()
        if self._delay != 0:
            time.sleep(self._delay)
        self.power_on()
        if self._sleep_after != 0:
            time.sleep(self._sleep_after)
        print("Unifi switch %s port %s now power cycled" % (self._sw_ip, self._sw_port))


########################################################################################################################
# main() function


def main():
    parser = argparse.ArgumentParser(description='Unifi Switch POE Control')

    parser.add_argument("--command", type=str, required=True, choices=['off', 'on', 'reboot'],
                        help='What you wish to do with the port')
    parser.add_argument("--delay", type=int, default=5,
                        help='Delay in seconds when rebooting between power off and power on (default 5 seconds)')
    parser.add_argument("--hostname", type=str, required=True, metavar="SWITCH",
                        help='The switch you wish to control - e.g. switch01 or the IP address')
    parser.add_argument("--port", type=int, required=True, metavar="PORT_NUMBER",
                        help='The switch port you wish to control, e.g. 4 or 15')
    parser.add_argument("--identity", type=str, default="unifi.key",
                        help='The SSH private key file to use for authentication (default "unifi.key")')
    parser.add_argument("--retries", type=int, default=3,
                        help='Number of times to retry setting the port (default 3)')
    parser.add_argument("--sleep_after", type=int, default=0,
                        help='Delay in seconds after command is executed (default 0)')
    parser.add_argument("--sleep_before", type=int, default=0,
                        help='Delay in seconds before command is executed (default 0)')
    parser.add_argument("--timeout", type=int, default=10,
                        help='Time in seconds to wait for command to fail (default 10 seconds)')
    parser.add_argument("--ssh_user", type=str, default="unifi",
                        help='Username for the ssh connection (default "unifi")')
    parser.add_argument("--ssh_port", type=int, default=22,
                        help='SSH port to connect to (default 22)')
    parser.add_argument("--ssh_prompt", type=str, default="USW-Pro-24*",
                        help='SSH connection prompt (default "USW-Pro-24*")')
    parser.add_argument("--prompt_switch_cli", type=str, default="(UBNT)*",
                        help='SSH connection prompt (default "(UBNT)*")')

    options = parser.parse_args()

    controller = UnifiPOECtrl(options.hostname, options.port, options.delay, options.sleep_before,
                              options.sleep_after, options.timeout, options.retries, options.identity,
                              options.ssh_user, options.ssh_port, options.ssh_prompt, options.prompt_switch_cli)

    switcher = {
        "off": controller.power_off,
        "on": controller.power_on,
        "reboot": controller.reboot,
        # "status": controller.get_port_state
    }

    switcher[options.command]()


########################################################################################################################

if __name__ == '__main__':
    main()
