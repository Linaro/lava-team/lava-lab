# installs some packages and creates nagios ssh authorized_keys for nagios checks

nagios:
  user.present:
    - fullname: Nagios Checks User
    - shell: /bin/bash
    - home: /home/nagios
    - password: "thisisnotapasswordhash"

/home/nagios/.ssh/:
  file.directory:
    - user: nagios
    - group: nagios
    - mode: 700

/home/nagios/.ssh/authorized_keys:
  file.managed:
    - source: salt://shared/nagios/authorized_keys
    - owner: nagios
    - mode: 600

nagios-plugins:
  pkg:
    - installed

nagios-plugins-contrib:
  pkg:
    - installed

/usr/lib/nagios/plugins/check_linux_raid:
  file.symlink:
    - target: /usr/lib/nagios/plugins/check_raid
    - force: True
