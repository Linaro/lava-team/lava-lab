#!/usr/bin/env python3

import argparse
import logging
import netrc
import os
import sys
import yaml
import jira
import requests
from netrc import netrc
from jira import JIRA
from urllib import parse
from urllib.parse import urlsplit

class NullAuth(requests.auth.AuthBase):
    '''force requests to ignore the ``.netrc``

    Some sites do not support regular authentication, but we still
    want to store credentials in the ``.netrc`` file and submit them
    as form elements. Without this, requests would otherwise use the
    .netrc which leads, on some sites, to a 401 error.

    Use with::

        requests.get(url, auth=NullAuth())

    Copied from: https://github.com/psf/requests/issues/2773#issuecomment-174312831
    '''

    def __call__(self, r):
        return r


def force_health_check(host, device, condition, error_msg, auth_dict):
    device["health"] = "Unknown"
    logging.info("Matched expr: \"%s\"" % condition['recheck_condition'])
    logging.info(
        "Last health check: %s"
        % device["last_health_report_job"]
    )
    logging.info("Health check error: %s" % error_msg)
    logging.info(
        "Forcing health check on %s" % device["hostname"]
    )
    device_state_update(host, device, error_msg, auth_dict)

def force_maintenance(host, device, condition, error_msg, auth_dict):
    device["health"] = "Maintenance"
    logging.info("%s reached the max number of repetitions of expr: \"%s\"" % (device["hostname"], condition['recheck_condition']))
    logging.info(
        "Last health check: %s"
        % device["last_health_report_job"]
    )
    logging.info("Health check error: %s" % error_msg)
    logging.info(
        "Putting %s in Maintenance and creating Jira ticket." % device["hostname"]
    )
    device_state_update(host, device, error_msg, auth_dict)


def device_state_update(host, device, error_msg, auth_dict):
    state_force_url = parse.urljoin(
        host, "api/v0.2/devices/%s/" % device["hostname"]
    )
    state_force_request = requests.put(
        state_force_url, device, headers=auth_dict, auth=NullAuth()
    )
    if state_force_request.status_code == 200:
        logging.info("Success!")
    else:
        logging.info("Failure")
        logging.debug(state_force_request.text)


def check_recheck_limit_exceeded(host, device, recheck_conditions, auth_dict):
    # Searching for submitter lava-health broke the API so it will be implemented more carefully if needed.
    payload = {'actual_device': device["hostname"], 'start_time__gt': '2009-09-09T00:00', 'ordering': '-start_time', 'limit': str(max(d['recheck_limit'] for d in recheck_conditions))}
    recheck_url = parse.urljoin(host, "api/v0.2/jobs/")
    logging.debug("Requesting %s" % recheck_url)
    recheck_jobs_request = requests.get(recheck_url, params=payload, headers=auth_dict, auth=NullAuth())
    if not recheck_jobs_request.status_code == 200:
        # If issues with the request.get has occured, return without action.
        logging.error("Request for %s failed, returned %s" % recheck_jobs_request.url, recheck_jobs_request)
        return

    rechecks = 0
    previous_error_pattern = ""
    #Latest error in database sort order.
    latest_error_message = ""
    #Get list of error conditions to recheck.
    recheck_pattern_list = error_pattern_list(recheck_conditions)
    recheck_jobs_request_json = recheck_jobs_request.json()
    for jobs in recheck_jobs_request_json["results"]:

        metadata = collect_metadata(host, device, jobs["id"], auth_dict)
        if not metadata:
            return
        # Successful run does not have error message: Catch key error
        try:
            error_msg = metadata["error_msg"]
        except KeyError:
            break

        if any(substring in error_msg for substring in recheck_pattern_list):
            for pattern in recheck_pattern_list:
                if pattern in error_msg:
                    if not latest_error_message:
                        latest_error_message = error_msg
                    if previous_error_pattern:
                        if not pattern == previous_error_pattern:
                            force_health_check(host, device, condition, latest_error_message, auth_dict)
                            return

                    previous_error_pattern = pattern
                    for condition in recheck_conditions:
                        if condition['recheck_condition'] == pattern:
                            rechecks = rechecks + 1
                            if rechecks >= condition['recheck_limit']:
                                #Sending ticket before forcing maintenance to prepare for future features.
                                send_jira_ticket(host, device, latest_error_message, auth_dict)
                                force_maintenance(host, device, condition, latest_error_message, auth_dict)
                                return
        else:
            return

    if previous_error_pattern:
        force_health_check(host, device, condition, error_msg, auth_dict)

def error_pattern_list(recheck_conditions):
    return [i["recheck_condition"] for i in recheck_conditions]

def collect_metadata(host, device, job_id, auth_dict):
    # Lookup suite ID
    lava_suite_url = parse.urljoin(
        host,
         "api/v0.2/jobs/%s/suites/?name=lava" % job_id
    )
    logging.debug("Requesting %s" % lava_suite_url)
    lava_suite_request = requests.get(
        lava_suite_url, headers=auth_dict, auth=NullAuth()
    )
    if lava_suite_request.status_code == 200:
    # there should only be one lava suite
    # lava job always contains lava suite in results
        lava_suite_request_json = lava_suite_request.json()
        if len(lava_suite_request_json["results"]) < 1:
            return False
        lava_suite = lava_suite_request_json["results"][0]
        # find job test case
        job_test_url = parse.urljoin(
            host,
            "api/v0.2/jobs/%s/suites/%s/tests/?name=job"
            % (job_id, lava_suite["id"]),
        )
        logging.debug("Requesting %s" % job_test_url)
        job_test_request = requests.get(job_test_url, headers=auth_dict, auth=NullAuth())
        if job_test_request.status_code == 200:
            job_tests = job_test_request.json()["results"]
            if len(job_tests) == 0:
                # most likely lava-logs crashed
                # force health check
                condition = {"recheck_condition": "No expr"}
                force_health_check(host, device, condition, "Missing error msg", auth_dict)
                return False
            return yaml.safe_load(job_tests[0]["metadata"])
    return False

def send_jira_ticket(host, device, error_msg, auth_dict):
    jira_host = "https://projects.linaro.org"
    username, _, password = netrc().authenticators(urlsplit(jira_host).netloc)
    jira_instance = JIRA(jira_host, auth=(username, password))
    fields = {
    "project": "LSS",
    "components": [{"name": "LAB"}],
    "summary": "%s on %s is put into maintenance" % (device["hostname"], host),
    "description": "Health check error: %s" % (error_msg),
    "issuetype": {"name": "Ticket"},
    "customfield_11010": [{"value": "LAB"}]
    }
    try:
        new_issue = jira_instance.create_issue(fields=fields)
    except:
        # Due to permission issues, ticket creation creates errors that breaks script unless caught.
        pass

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("--host", required=True, help="LAVA host URL")
    parser.add_argument("--strings", type=str, default="restart_strings.yaml",
                        help="List of strings if appearing in error should trigger a hc rerun")
    parser.add_argument(
        "--token", default=os.environ.get("LAVA_TOKEN"), help="LAVA access token",
    )
    parser.add_argument(
        "--loglevel",
        default="INFO",
        choices=["DEBUG", "INFO", "WARNING", "ERROR", "CRITICAL"],
        help="Log level",
    )

    args = parser.parse_args()
    logging.basicConfig(level=logging.getLevelName(args.loglevel))

    host_parts = parse.urlparse(args.host)
    host = "%s://%s" % (host_parts.scheme, host_parts.netloc)

    with open(args.strings) as f:
        recheck_conditions = yaml.safe_load(f)['strings']

    auth_dict = {"Authorization": "Token %s" % args.token}
    if args.token == None:
        # try token from .netrc
        try:
            netrcauth = netrc()
            username, _, token = netrcauth.authenticators(host_parts.netloc)
            auth_dict = {"Authorization": "Token %s" % token}
        except FileNotFoundError:
            logging.warning(".netrc file not available")
            sys.exit(1)
        except TypeError:
            logging.warning("No token available")
            sys.exit(1)

    bad_device_url = parse.urljoin(host, "api/v0.2/devices/?health=Bad")
    logging.debug("Requesting %s" % bad_device_url)
    bad_devices_request = requests.get(bad_device_url, headers=auth_dict, auth=NullAuth())
    if bad_devices_request.status_code == 200:
        bad_devices_json = bad_devices_request.json()
        for device in bad_devices_json["results"]:
            # collect metadata is only used to force health check if the no lava_suite condition is met.
            if not collect_metadata(host, device, device["last_health_report_job"], auth_dict):
                continue

            check_recheck_limit_exceeded(host, device, recheck_conditions, auth_dict)
