# keep important /etc configs (e.g. ser2net and lava-slave)
# WARNING: Be very careful about using clean: True. If you do you
# will completely blow away the whole tree (e.g./etc/apt)

/etc/lava-dispatcher/lava-slave:
  file.managed:
    - source: salt://common-configs/lava-dispatcher/lava-slave

lava-slave:
  service.running:
    - enable: True
    - watch:
      - file: /etc/lava-dispatcher/lava-slave

/etc/apt:
  file.recurse:
    - source: salt://common-configs/apt
    - clean: False
    - include_empty: True

/etc/localtime:
  file.symlink:
    - target: /usr/share/zoneinfo/UTC

/etc/lava-coordinator/:
  file.directory:
    - user: root
    - group: root
    - mode: 755

expect:
  pkg:
    - installed

ipmitool:
  pkg:
    - installed

ntp:
  pkg:
    - installed

simg2img:
  pkg:
    - installed

img2simg:
  pkg:
    - installed

libusb-1.0-0:
  pkg:
    - installed

python3-usb:
  pkg:
    - installed

libftdi1-2:
  pkg:
    - installed

docker-ce:
  pkg:
    - installed

apt-transport-https:
  pkg:
    - installed

ca-certificates:
  pkg:
    - installed

curl:
  pkg:
    - installed

gnupg2:
  pkg:
    - installed

software-properties-common:
  pkg:
    - installed

python3-pip:
  pkg:
    - installed

install-sentry-sdk:
  cmd.run:
    - name: python3 -m pip install sentry-sdk

# Removed for now - will re-instate when Buster is deployed lab wide
#python-pyocd:
#  pkg:
#    - installed
